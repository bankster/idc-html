  <footer class="">
  	<div class="container " id="containerFooter">
      <div>&copy; สงวนลิขสิทธิ์ 2014 บริษัท อินโนเวทีฟ ดีไซน์ แอนด์ ครีเอชั่น จำกัด</div>
    </div>
  </footer>
  <!-- /#containerFooter -->

  <script src="js/vendor/jquery-1.11.0.min.js"></script>
  <script src="js/vendor/bootstrap.min.js"></script>
  <script src="js/vendor/owl.carousel.min.js"></script>
  <script src="js/vendor/holder.js"></script>

  <script src="js/plugins.js"></script>
  <script src="js/main.js"></script>