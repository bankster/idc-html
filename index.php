<?php
	$head = file_get_contents('_head.php');
	$header = file_get_contents('_header.php');
	// $content = file_get_contents('content/home.php');
	$footer = file_get_contents('_footer.php');
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
  <?php echo $head; ?>
</head>
<body>
	<?php echo $header; ?>

  <div class="container" id="containerContent">
  	<div class="slide-container">
  		<div class="owl-carousel">
  			<div>
  				<img src="holder.js/100%x400/sky" alt="">
  			</div>
  			<div>
  				<img src="holder.js/100%x400/vine" alt="">
  			</div>
  			<div>
  				<img src="holder.js/100%x400/lava" alt="">
  			</div>
  			<div>
  				<img src="holder.js/100%x400/social" alt="">
  			</div>
  		</div>
  		<!-- /.owl-carousel -->
  	</div>
  	<!-- /.slide-container -->
    <div class="content">
	    <div class="recommend-container row">
	    	<div class="col-xs-4">
	      	<div class="recommend">
	      		<div class="recommend-img">
	      			<img src="holder.js/100%x200" alt="recommend img">
	      		</div>
			      <div class="recommend-title">
			        <h3>Recommend title</h3>
			      </div>
			      <div class="recommend-content">
			      	<p>detail</p>
			      </div>
			      <div class="recommend-link">
			      	<a href="#rec-1">recommend link</a>
			      </div>
			    </div>
			    <!-- /.recommend -->
	      </div>
	      <!-- /.col-xs-4 -->
	      <div class="col-xs-4">
	      	<div class="recommend">
	      		<div class="recommend-img">
	      			<img src="holder.js/100%x200" alt="recommend img">
	      		</div>
			      <div class="recommend-title">
			        <h3>Recommend title</h3>
			      </div>
			      <div class="recommend-content">
			      	<p>detail</p>
			      </div>
			      <div class="recommend-link">
			      	<a href="#rec-1">recommend link</a>
			      </div>
			    </div>
			    <!-- /.recommend -->
	      </div>
	      <!-- /.col-xs-4 -->
	      <div class="col-xs-4">
	      	<div class="recommend">
	      		<div class="recommend-img">
	      			<img src="holder.js/100%x200" alt="recommend img">
	      		</div>
			      <div class="recommend-title">
			        <h3>Recommend title</h3>
			      </div>
			      <div class="recommend-content">
			      	<p>detail</p>
			      </div>
			      <div class="recommend-link">
			      	<a href="#rec-1">recommend link</a>
			      </div>
			    </div>
			    <!-- /.recommend -->
	      </div>
	      <!-- /.col-xs-4 -->
	    </div>
	    <!-- /.recommend-container -->

	    <div class="portfolio-container">
	    	<h2>Portfolio</h2>
				<div class="row">
					<div class="col-xs-3">
						<img src="holder.js/100%x180" alt="project cover">
						<h5>Project name</h5>
					</div>
					<div class="col-xs-3">
						<img src="holder.js/100%x180" alt="project cover">
						<h5>Project name</h5>
					</div>
					<div class="col-xs-3">
						<img src="holder.js/100%x180" alt="project cover">
						<h5>Project name</h5>
					</div>
					<div class="col-xs-3">
						<img src="holder.js/100%x180" alt="project cover">
						<h5>Project name</h5>
					</div>
				</div>
	    </div>
    </div>
  </div>
	<!-- /.content -->
	<?php echo $footer; ?>
</body>
</html>