<?php
	$head = file_get_contents('_head.php');
	$header = file_get_contents('_header.php');
	// $content = file_get_contents('content/home.php');
	$footer = file_get_contents('_footer.php');
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
  <?php echo $head; ?>
</head>
<body>
	<?php echo $header; ?>

  <div class="container" id="containerContent">
    <div class="content">
			<div class="page-header">
				<h1>News</h1>
			</div>
			<div class="row">
				<div class="col-md-8">
					<div class="article-container">
						
						<?php for($i=1; $i<=5; $i++) : ?>

						<div class="media">
							<div class="pull-left">
								<a href="#a1">
									<img src="holder.js/120x120" alt="article img">
								</a>
							</div>
							<div class="media-body">
								<h1 class="media-heading">
									<a href="#a1">Article name</a>
								</h1>
								<p>
									Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
								</p>
							</div>
						</div>
						<!-- /.media -->

						<?php endfor; ?>

			    </div>
			    <!-- /.article-container -->
			    <ul class="pagination">
					  <li><a href="#">&laquo;</a></li>
					  <li><a href="#">1</a></li>
					  <li><a href="#">2</a></li>
					  <li><a href="#">3</a></li>
					  <li><a href="#">4</a></li>
					  <li><a href="#">5</a></li>
					  <li><a href="#">&raquo;</a></li>
					</ul>
					<!-- /.pagination -->
				</div>
				<aside class="col-md-4">
					<div class="widget">
						<div class="widget-heading">
							<h2>Widget Title</h2>
						</div>
						<div class="widget-content">
							<ul>
								<li><a href="#c1">Category 1</a></li>
								<li><a href="#c2">Category 2</a></li>
								<li><a href="#c3">Category 3</a></li>
								<li><a href="#c4">Category 4</a></li>
								<li><a href="#c5">Category 5</a></li>
							</ul>
						</div>
					</div>
					<!-- /.widget -->

					<div class="widget">
						<div class="widget-heading">
							<h2>Widget Title</h2>
						</div>
						<div class="widget-content">
							<ul>
								<li><a href="#c1">Category 1</a></li>
								<li><a href="#c2">Category 2</a></li>
								<li><a href="#c3">Category 3</a></li>
								<li><a href="#c4">Category 4</a></li>
								<li><a href="#c5">Category 5</a></li>
							</ul>
						</div>
					</div>
					<!-- /.widget -->
				</aside>
			</div>
	    <!-- /.row -->
    </div>
    <!-- /.content -->
  </div>
	<!-- /#containerContent -->
	<?php echo $footer; ?>
</body>
</html>