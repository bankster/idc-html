<?php
	$head = file_get_contents('_head.php');
	$header = file_get_contents('_header.php');
	// $content = file_get_contents('content/home.php');
	$footer = file_get_contents('_footer.php');
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
  <?php echo $head; ?>
</head>
<body>
	<?php echo $header; ?>

  <div class="container" id="containerContent">
    <div class="content">
			<div class="page-header">
				<h1>Contact</h1>
			</div>
	    <div class="contact-container">
				<div class="row">
					<div class="col-md-8">
						<div class="contact-info">
							<h2>Company name</h2>
							<p>
								company description
							</p>
							<h3>Address</h3>
							<p>209/23 ถนนวัวลาย ตำบลหายยา
	อำเภอเมือง จังหวัดเชียงใหม่</p>
							<h3>Phone</h3>
							<p>099-999-9999</p>
							<h3>Email</h3>
							<p>contact@sitename.com</p>
						</div>
						<div class="contact-from">
							<form action="" role="form">
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
									    <label for="name">Name</label>
									    <input type="name" class="form-control" id="name" placeholder="Enter name">
									  </div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
									    <label for="email">Email</label>
									    <input type="email" class="form-control" id="email" placeholder="Enter email">
									  </div>
									</div>
									<div class="col-md-12">
										<div class="form-group">
									    <label for="message">Message</label>
									    <textarea name="message" class="form-control" id="message" rows="10"></textarea>
									  </div>
									  <button type="submit" class="btn btn-default">Submit</button>
									</div>
								</div>
								<!-- /.row -->
							</form>
						</div>
						<!-- /.contact-form -->
					</div>
					<div class="col-md-4">
						<img src="holder.js/100%x600" alt="">
					</div>
				</div>
	    </div>

    </div>
  </div>
	<!-- /#containerContent -->
	<?php echo $footer; ?>
</body>
</html>