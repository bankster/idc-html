<?php
	$head = file_get_contents('_head.php');
	$header = file_get_contents('_header.php');
	// $content = file_get_contents('content/home.php');
	$footer = file_get_contents('_footer.php');
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
  <?php echo $head; ?>
</head>
<body>
	<?php echo $header; ?>

  <div class="container" id="containerContent">
    <div class="content">
			<div class="page-header">
				<h1>Portfolio</h1>
			</div>
	    <div class="portfolio-container">
				<div class="row">
					<div class="col-xs-3">
						<img src="holder.js/100%x180" alt="project cover">
						<h5>Project name</h5>
					</div>
					<div class="col-xs-3">
						<img src="holder.js/100%x180" alt="project cover">
						<h5>Project name</h5>
					</div>
					<div class="col-xs-3">
						<img src="holder.js/100%x180" alt="project cover">
						<h5>Project name</h5>
					</div>
					<div class="col-xs-3">
						<img src="holder.js/100%x180" alt="project cover">
						<h5>Project name</h5>
					</div>

					<div class="clearfix"></div>

					<div class="col-xs-3">
						<img src="holder.js/100%x180" alt="project cover">
						<h5>Project name</h5>
					</div>
					<div class="col-xs-3">
						<img src="holder.js/100%x180" alt="project cover">
						<h5>Project name</h5>
					</div>
					<div class="col-xs-3">
						<img src="holder.js/100%x180" alt="project cover">
						<h5>Project name</h5>
					</div>
					<div class="col-xs-3">
						<img src="holder.js/100%x180" alt="project cover">
						<h5>Project name</h5>
					</div>

					<div class="clearfix"></div>

					<div class="col-xs-3">
						<img src="holder.js/100%x180" alt="project cover">
						<h5>Project name</h5>
					</div>
					<div class="col-xs-3">
						<img src="holder.js/100%x180" alt="project cover">
						<h5>Project name</h5>
					</div>
					<div class="col-xs-3">
						<img src="holder.js/100%x180" alt="project cover">
						<h5>Project name</h5>
					</div>
					<div class="col-xs-3">
						<img src="holder.js/100%x180" alt="project cover">
						<h5>Project name</h5>
					</div>
				</div>
	    </div>

    </div>
  </div>
	<!-- /#containerContent -->
	<?php echo $footer; ?>
</body>
</html>